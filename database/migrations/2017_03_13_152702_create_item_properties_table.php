<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemPropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_properties', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('property_value_id')->unsigned()->foreign()->references('id')->on('property_values');
            // $table->foreign('property_value_id')->references('id')->on('property_values');
            $table->integer('property_name_id')->unsigned()->foreign()->references('id')->on('property_names');
            //$table->foreign('property_name_id')->references('id')->on('property_names');
            $table->integer('item_id')->unsigned();
            $table->foreign('item_id')->references('id')->on('items');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_properties');
    }
}
