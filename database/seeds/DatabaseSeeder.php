<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $admin=new \App\User();
        $admin->name='admin';
        $admin->email='lapchenkovbyu@gmail.com';
        $admin->role='admin';
        $admin->password=Hash::make('123123');
        $admin->save();

        $user=new \App\User();
        $user->name='user';
        $user->email='123@gmail.com';
        $user->password=Hash::make('123123');
        $user->save();

        $category=new \App\Category();
        $category->name="Computers";
        $category->description="description";
        $category->save();

        $item=new \App\Item();
        $item->name="Asus 12456";
        $item->category_id="1";
        $item->image="image";
        $item->description="Описание";
        $item->price="1245";
        $item->save();

        $property_name=new \App\PropertyName();
        $property_name->name="processor";
        $property_name->description="other";
        $property_name->save();

        $property_preference=new \App\PropertyPreference();
        $property_preference->category_id="1";
        $property_preference->property_name_description='{"description": ["processor, DDR, OPM"]}';
        $property_preference->save();

        $property_value=new \App\PropertyValue();
        $property_value->property_name_id="1";
        $property_value->value="4000 mb";
        $property_value->save();

        $item_property=new \App\ItemProperty();
        $item_property->property_value_id="1";
        $item_property->property_name_id="1";
        $item_property->item_id="1";
        $item_property->save();



    }
}
