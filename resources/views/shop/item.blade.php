@include('shop.layouts.header')
{{--@include('shop.layouts.slider')--}}
<div class="items">
<div class="items-sec btm-sec">
				@foreach($items as $item)
			 	<div class="col-md-1 feature-grid">
					<a href="#"><img src="{{$item->image}}" alt="{{$item->name}}"/>
					<div class="arrival-info">
						<h4 class="text-center">{{$item->name}}</h4>
						<form method="post" action="/add_to_cart">
							<input type="hidden" name="item_id" value="{{$item->id}}">
							{{csrf_field()}}
							<div class="text-center">
							<button type="submit" class="btn btn-primary">ADD TO CART</button>
							</div>
						</form>
					</div>
					<div class="viw">
						<a href="#"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>View</a>
					</div>
				  	</a>
			 	</div>
			 	@endforeach
			 	<div class="clearfix"></div>
			</div>

</div>

<!--- -->
<div class="subscribe">
	 <div class="container">
		 <h3>Newsletter</h3>
		 <form>
			 <input type="text" class="text" value="Email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}">
			 <input type="submit" value="Subscribe">
		 </form>
	 </div>
</div>
<!---->
@include('shop.layouts.footer')