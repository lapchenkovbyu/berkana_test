<!DOCTYPE html>
<html>
<head>
    <title>Berkana Shop</title>
    <link href="/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
    <!-- Custom Theme files -->
    <!--theme style-->
    <link href="/css/style.css" rel="stylesheet" type="text/css" media="all" />
    <script src="/js/jquery.min.js"></script>

    <!--//theme style-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- start menu -->
    <script src="/js/simpleCart.min.js"> </script>
    <!-- start menu -->
    <link href="/css/memenu.css" rel="stylesheet" type="text/css" media="all" />
    <script type="text/javascript" src="/js/memenu.js"></script>
    <script>$(document).ready(function(){$(".memenu").memenu();});</script>
    <!-- /start menu -->
</head>
<body>
<!--header-->
<script src="/js/responsiveslides.min.js"></script>
<script>
    $(function () {
        $("#slider").responsiveSlides({
            auto: true,
            nav: true,
            speed: 500,
            namespace: "callbacks",
            pager: false,
        });
    });
</script>
<script src="js/bootstrap.js"> </script>
<div class="header-top">
    <div class="header-bottom">
        <div class="logo">
            <h1><a href="/">
                    {{--<img src="/images/berkana.png">--}}
                Berkana
                </a></h1>
        </div>
        <!---->
        <div class="top-nav">
            <ul class="memenu skyblue"><li class="active"><a href="/">Home</a></li>
                <li class="grid">
                    <a href="#">Products</a>
                    <div class="mepanel">
                        <div class="row">
                            <div class="col1 me-one">
                                <h4>Shop</h4>
                                <ul>
                                    <li><a href="product.html">New Arrivals</a></li>
                                    <li><a href="product.html">Home</a></li>
                                    <li><a href="product.html">Decorates</a></li>
                                    <li><a href="product.html">Accessories</a></li>
                                    <li><a href="product.html">Kids</a></li>
                                    <li><a href="login">Login</a></li>
                                    <li><a href="product.html">Brands</a></li>
                                    <li><a href="product.html">My Shopping Bag</a></li>
                                </ul>
                            </div>
                            <div class="col1 me-one">
                                <h4>Type</h4>
                                <ul>
                                    <li><a href="product.html">Diwali Lights</a></li>
                                    <li><a href="product.html">Tube Lights</a></li>
                                    <li><a href="product.html">Bulbs</a></li>
                                    <li><a href="product.html">Ceiling Lights</a></li>
                                    <li><a href="product.html">Accessories</a></li>
                                    <li><a href="product.html">Lanterns</a></li>
                                </ul>
                            </div>
                            <div class="col1 me-one">
                                <h4>Popular Brands</h4>
                                <ul>
                                    <li><a href="product.html">Everyday</a></li>
                                    <li><a href="product.html">Philips</a></li>
                                    <li><a href="product.html">Havells</a></li>
                                    <li><a href="product.html">Wipro</a></li>
                                    <li><a href="product.html">Jaguar</a></li>
                                    <li><a href="product.html">Ave</a></li>
                                    <li><a href="product.html">Gold Medal</a></li>
                                    <li><a href="product.html">Anchor</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="grid"><a href="#">Accessories</a>
                    <div class="mepanel">
                        <div class="row">
                            <div class="col1 me-one">
                                <h4>Shop</h4>
                                <ul>
                                    <li><a href="product.html">New Arrivals</a></li>
                                    <li><a href="product.html">Home</a></li>
                                    <li><a href="product.html">Decorates</a></li>
                                    <li><a href="product.html">Accessories</a></li>
                                    <li><a href="product.html">Kids</a></li>
                                    <li><a href="product.html">Login</a></li>
                                    <li><a href="product.html">Brands</a></li>
                                    <li><a href="product.html">My Shopping Bag</a></li>
                                </ul>
                            </div>
                            <div class="col1 me-one">
                                <h4>Type</h4>
                                <ul>
                                    <li><a href="product.html">Diwali Lights</a></li>
                                    <li><a href="product.html">Tube Lights</a></li>
                                    <li><a href="product.html">Bulbs</a></li>
                                    <li><a href="product.html">Ceiling Lights</a></li>
                                    <li><a href="product.html">Accessories</a></li>
                                    <li><a href="product.html">Lanterns</a></li>
                                </ul>
                            </div>
                            <div class="col1 me-one">
                                <h4>Popular Brands</h4>
                                <ul>
                                    <li><a href="product.html">Everyday</a></li>
                                    <li><a href="product.html">Philips</a></li>
                                    <li><a href="product.html">Havells</a></li>
                                    <li><a href="product.html">Wipro</a></li>
                                    <li><a href="product.html">Jaguar</a></li>
                                    <li><a href="product.html">Ave</a></li>
                                    <li><a href="product.html">Gold Medal</a></li>
                                    <li><a href="product.html">Anchor</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </li>
                {{--<li class="grid"><a href="typo.html">Typo</a></li>--}}
                <li class="grid"><a href="contact.html">Contact</a></li>
            </ul>
        </div>
        <!---->
        <div class="cart box_1">
            <a href="checkout.html">
                <div class="total">
                    <span class="simpleCart_total"></span> (<span id="simpleCart_quantity" class="simpleCart_quantity"></span>)</div>
                <span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span>
            </a>
            <p><a href="javascript:;" class="simpleCart_empty">Empty Cart</a></p>
            <div class="clearfix"> </div>
        </div>
        <div class="clearfix"> </div>
        <!---->
    </div>
    <div class="clearfix"> </div>
</div>
<!---->