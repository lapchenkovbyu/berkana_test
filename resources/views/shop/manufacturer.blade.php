@include('shop.layouts.header')

<div class="items">
	 <div class="container">

			 <div>
			 <br>
			 <h2 class="text-center">Brands</h2>
			 <br>
		 </div>
		 @foreach($manufacturer as $man)
		 <div class="items-sec">
			 <div class="col-md-3 feature-grid">
				 <a href="#"><img src="/" alt=""/>
					 <div class="arrival-info">
						 <h4 class="text-center">{{$man->name}}</h4>
					 </div>
					 <div class="viw">
						<a href="#">
							<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>View</a>
					 </div>
				  </a>
			 </div>
			 <div class="clearfix"></div>
		 </div>
		 @endforeach
				 <div>
			 <br>
			 <h2 class="text-center">Items</h2>
			 <br>
		 </div>
		 <div class="items-sec btm-sec">
			 <div class="col-md-3 feature-grid">
				 <a href="#"><img src="/" alt=""/>
					 <div class="arrival-info">
						 <h4 class="text-center"></h4>
						 <form method="post" action="/add_to_cart">
							 <input type="hidden" name="item_id" value="">
							 <div class="text-center">
								 <button type="submit" class="btn btn-primary">ADD TO CART</button>
							 </div>
						 </form>
					 </div>
					 <div class="viw">
						<a href="#"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>View</a>
					 </div>
				  </a>
			 </div>
			 <div class="clearfix"></div>
		 </div>
	 </div>
</div>
<div class="col-md-12 text-center">
	<h1></h1>
	<div class="col-md-4">
		<img src="/" height="300px">
	</div>
	<div class="col-md-8">
		<p>
		</p>
	</div>
</div>
<!---->
<div class="offers">
	 <div class="container">
		 <div class="clearfix"></div>
	 </div>
	 </div>
</div>
<!---->
<div class="subscribe">
	 <div class="container">
		 <h3>Newsletter</h3>
		 <form>
			 <input type="text" class="text" value="Email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}">
			 <input type="submit" value="Subscribe">
		 </form>
	 </div>
</div>

