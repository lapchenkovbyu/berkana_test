@include('shop.layouts.header')
{{--@include('shop.layouts.slider')--}}
<div class="items">
	 <div class="container">
		@if(@isset($category_show))
			<div>
				<br>
				<h2 class="text-center">Categories</h2>
				<br>
			</div>
			<div class="items-sec">
				@foreach($category_show as $cat)
			 		<div class="col-md-3 feature-grid">
				 		<a href="/category/{{$cat->id}}"><img src="/{{$cat->image}}" alt=""/>
					 		<div class="arrival-info">
						 		<h4 class="text-center">{{$cat->name}}</h4>
					 		</div>
					 		<div class="viw">
								<a href="/category/{{$cat->id}}">
								<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>View</a>
					 		</div>
				  		</a>
			 		</div>
			 	@endforeach
			 	<div class="clearfix"></div>
			</div>
		@endif
		@if(@isset($categories))
		 	<div>
				<br>
				<h2 class="text-center">Category: {{$categories->name}}</h2>
				<br>
		 	</div>
		 	@if($subcategories->isNotEmpty())
			 	<div>
					<br>
					<h2 class="text-center">Subcategories</h2>
					<br>
				</div>
				<div class="items-sec">
					@foreach($subcategories as $subcategory)
				 		<div class="col-md-3 feature-grid">
					 		<a href="/category/{{$subcategory->id}}"><img src="/{{$subcategory->image}}" alt=""/>
						 		<div class="arrival-info">
							 		<h4 class="text-center">{{$subcategory->name}}</h4>
						 		</div>
						 		<div class="viw">
									<a href="/category/{{$subcategory->id}}">
									<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>View</a>
						 		</div>
					  		</a>
				 		</div>
				 	@endforeach
				 	<div class="clearfix"></div>
				</div>
			@endif
			@if($items->isNotEmpty())
				<div>
					<br>
					<h2 class="text-center">Items</h2>
					<br>
				</div>
		 		<div class="items-sec btm-sec">
					@foreach($items as $item)
			 		<div class="col-md-4 feature-grid">
						<a href="#"><img src="/{{$item->image}}" alt="{{$item->name}}"/>
						<div class="arrival-info">
							<h4 class="text-center">{{$item->name}}</h4>
							<form method="post" action="/add_to_cart">
								<input type="hidden" name="item_id" value="{{$item->id}}">
								{{csrf_field()}}
								<div class="text-center">
								<button type="submit" class="btn btn-primary">ADD TO CART</button>
								</div>
							</form>
						</div>
						<div class="viw">
							<a href="#"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>View</a>
						</div>
					  	</a>
				 	</div>
				 	@endforeach
				 	<div class="clearfix"></div>
				</div>
			@endif
		@endif
	 </div>
</div>

<!---->
<div class="offers">
	 <div class="container">
	 {{--<h3>End of Season Sale</h3>--}}
	 <div class="offer-grids">
		 {{--<div class="col-md-6 grid-left">--}}
			 {{--<a href="#"><div class="offer-grid1">--}}
				 {{--<div class="ofr-pic">--}}
					 {{--<img src="images/ofr2.jpeg" class="img-responsive" alt=""/>--}}
				 {{--</div>--}}
				 {{--<div class="ofr-pic-info">--}}
					 {{--<h4>Emergency Lights <br>& Led Bulds</h4>--}}
					 {{--<span>UP TO 60% OFF</span>--}}
					 {{--<p>Shop Now</p>--}}
				 {{--</div>--}}
				 {{--<div class="clearfix"></div>--}}
			 {{--</div></a>--}}
		 {{--</div>--}}
		 {{--<div class="col-md-6 grid-right">--}}
			 {{--<a href="#"><div class="offer-grid2">--}}
				 {{--<div class="ofr-pic-info2">--}}
					 {{--<h4>Flat Discount</h4>--}}
					 {{--<span>UP TO 30% OFF</span>--}}
					 {{--<h5>Outdoor Gate Lights</h5>--}}
					 {{--<p>Shop Now</p>--}}
				 {{--</div>--}}
				 {{--<div class="ofr-pic2">--}}
					 {{--<img src="images/ofr3.jpg" class="img-responsive" alt=""/>--}}
				 {{--</div>--}}
				 {{--<div class="clearfix"></div>--}}
			 {{--</div></a>--}}
		 {{--</div>--}}
		 <div class="clearfix"></div>
	 </div>
	 </div>
</div>
<!---->
<div class="subscribe">
	 <div class="container">
		 <h3>Newsletter</h3>
		 <form>
			 <input type="text" class="text" value="Email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}">
			 <input type="submit" value="Subscribe">
		 </form>
	 </div>
</div>
<!---->
@include('shop.layouts.footer')