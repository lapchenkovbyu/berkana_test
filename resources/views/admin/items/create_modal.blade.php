<!-- Modal -->
<div class="modal fade" id="createitem" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Create item</h4>
            </div>
            <form method="post" action="/admin/items/create" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="modal-body">
                    <div class="form-group">
                        <label>Name</label>
                        <br>
                        <input class="form-control" type="text" placeholder="Name" name="name">
                    </div>
                    <div class="form-group">
                        <label>Manufactures</label>
                        <br>
                        <select class="form-control" name="manufacturer_id">
                            <option disabled selected>Please select manufacturer</option>
                            @foreach($manufacturers as $m)
                                <option value="{{$m->id}}">
                                    {{$m->name}}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Image</label>
                        <br>
                        <input class="form-control" type="file" placeholder="Image" name="image">
                    </div>
                    <div class="form-group">
                        <label>Parent category</label>
                        <br>
                        <select class="form-control" name="category_id">
                             @if($category!=null)
                                <option selected value="{{$category->id}}">
                                    {{$category->name}}
                                </option>
                                @foreach($categories as $cat)
                                @if($cat->id != $category->id)
                                <option value="{{$cat->id}}">
                                    {{$cat->name}}
                                </option>
                                @endif
                            @endforeach
                            @else
                                <option disabled selected>Please select parent category</option>
                                @foreach($categories as $cat)
                                    <option value="{{$cat->id}}">
                                        {{$cat->name}}
                                    </option>
                                @endforeach
                            @endif
                            
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Description</label>
                        <br>
                        <textarea class="form-control" name="description" placeholder="Description"></textarea>
                    </div>
                    <div class="form-group">
                      <!-- Nav tabs -->
                      <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Home</a></li>
                        <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Profile</a></li>
                        <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Messages</a></li>
                        <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Settings</a></li>
                      </ul>

                      <!-- Tab panes -->
                      <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="home">home</div>
                        <div role="tabpanel" class="tab-pane" id="profile">profile</div>
                        <div role="tabpanel" class="tab-pane" id="messages">messages</div>
                        <div role="tabpanel" class="tab-pane" id="settings">settings</div>
                      </div>

                    </div>
                    <!-- <div class="form-group">
                            <div class=" bs-example bs-example-tabs" data-example-id="togglable-tabs">
                                <ul class="nav nav-tabs" id="myTabs" role="tablist">
                                    <li role="presentation" class=" dropdown active">
                                        <a href="#" class=" dropdown-toggle" id="myTabDrop1" data-toggle="dropdown" aria-controls="myTabDrop1-contents" aria-expanded="false">
                                            Dropdown
                                            <span class="caret"></span>
                                        </a>
                                        <ul class="dropdown-menu" aria-labelledby="myTabDrop1" id="myTabDrop1-contents">
                                            <li class="">
                                                <a href="#dropdown1" role="tab" id="dropdown1-tab" data-toggle="tab" aria-controls="dropdown1" aria-expanded="false">
                                                    @fat
                                                </a>
                                            </li>
                                            <li class="active">
                                                <a href="#dropdown2" role="tab" id="dropdown2-tab" data-toggle="tab" aria-controls="dropdown2" aria-expanded="true">
                                                    @mdo
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                                <div class="tab-content" id="myTabContent">
                                    <div class="tab-pane fade" role="tabpanel" id="home" aria-labelledby="home-tab">    <p>
                                            Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua, retro synth master cleanse. Mustache cliche tempor, williamsburg carles vegan helvetica. Reprehenderit butcher retro keffiyeh dreamcatcher synth. Cosby sweater eu banh mi, qui irure terry richardson ex squid. Aliquip placeat salvia cillum iphone. Seitan aliquip quis cardigan american apparel, butcher voluptate nisi qui.
                                        </p>
                                    </div>
                                    <div class="tab-pane fade" role="tabpanel" id="profile" aria-labelledby="profile-tab">
                                        <p>
                                            Food truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid. Exercitation +1 labore velit, blog sartorial PBR leggings next level wes anderson artisan four loko farm-to-table craft beer twee. Qui photo booth letterpress, commodo enim craft beer mlkshk aliquip jean shorts ullamco ad vinyl cillum PBR. Homo nostrud organic, assumenda labore aesthetic magna delectus mollit. Keytar helvetica VHS salvia yr, vero magna velit sapiente labore stumptown. Vegan fanny pack odio cillum wes anderson 8-bit, sustainable jean shorts beard ut DIY ethical culpa terry richardson biodiesel. Art party scenester stumptown, tumblr butcher vero sint qui sapiente accusamus tattooed echo park.
                                        </p>
                                    </div>
                                    <div class="tab-pane fade" role="tabpanel" id="dropdown1" aria-labelledby="dropdown1-tab">
                                        <p>
                                            Etsy mixtape wayfarers, ethical wes anderson tofu before they sold out mcsweeney's organic lomo retro fanny pack lo-fi farm-to-table readymade. Messenger bag gentrify pitchfork tattooed craft beer, iphone skateboard locavore carles etsy salvia banksy hoodie helvetica. DIY synth PBR banksy irony. Leggings gentrify squid 8-bit cred pitchfork. Williamsburg banh mi whatever gluten-free, carles pitchfork biodiesel fixie etsy retro mlkshk vice blog. Scenester cred you probably haven't heard of them, vinyl craft beer blog stumptown. Pitchfork sustainable tofu synth chambray yr.
                                        </p>
                                    </div>
                                    <div class="tab-pane fade active in" role="tabpanel" id="dropdown2" aria-labelledby="dropdown2-tab">
                                        <p>
                                            Trust fund seitan letterpress, keytar raw denim keffiyeh etsy art party before they sold out master cleanse gluten-free squid scenester freegan cosby sweater. Fanny pack portland seitan DIY, art party locavore wolf cliche high life echo park Austin. Cred vinyl keffiyeh DIY salvia PBR, banh mi before they sold out farm-to-table VHS viral locavore cosby sweater. Lomo wolf viral, mustache readymade thundercats keffiyeh craft beer marfa ethical. Wolf salvia freegan, sartorial keffiyeh echo park vegan.
                                        </p>
                                    </div>
                                    </div> </div>

                            @if($category!=null)
                                @foreach($property as $prop)
                                    <label>{{ $prop }}</label>
                                    <textarea class="form-control" name="{{$prop}}" placeholder="Description"></textarea>
                                @endforeach
                            @endif
                    </div> -->
                    <div class="form-group">
                        <label>Price</label>
                        <br>
                        <input class="form-control" type="number" name="price" placeholder="Price" min="0">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>