<!-- Button trigger modal -->
<button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#edit-sub{{$subcategory->id}}">
    Edit subcategory
</button>

<!-- Modal -->
<div class="modal fade" id="edit-sub{{$subcategory->id}}">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Editing category #{{$subcategory->id}}</h4>
            </div>
            <form method="post" action="/admin/categories/edit" enctype="multipart/form-data">
                {{csrf_field()}}
                <input type="hidden" name="id" value="{{$subcategory->id}}">
                <div class="modal-body">
                    <div class="form-group">
                        <label>Name</label>
                        <br>
                        <input class="form-control" type="text" name="name" value="{{$subcategory->name}}">
                    </div>
                    <div class="form-group">
                        <label>Image</label>
                        <br>
                        <input class="form-control" type="file" name="image" value="{{$subcategory->image}}">
                    </div>
                    <div class="form-group">
                        <label>Parent category</label>
                        <br>
                        <select class="form-control" name="parent_category_id">
                            <option disabled selected>Please select parent category</option>
                            @foreach($categories as $category1)
                                <option value="{{$category1->id}}">
                                    {{$category1->name}}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Description</label>
                        <br>
                        <textarea class="form-control" name="description">{{$subcategory->description}}</textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>