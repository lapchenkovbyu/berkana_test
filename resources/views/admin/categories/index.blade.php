@extends('layouts.app')

@section('content')
    @include('admin.categories.create_modal')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="text-center">
                    <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#createcategory">+ New</button>
                    <h1>Categories
                    </h1>
                </div>
                @foreach($displayed_categories as $category)
                    <div class="panel panel-default">
                        <div class="panel-heading"><a href="/admin/categories/show/{{$category->id}}">{{$category->name}}</a></div>

                        <div class="panel-body">
                            <div class="col-md-3">
                                <img src="/{{$category->image}}" width="230px">
                            </div>
                            <div class="col-md-8">
                                {{$category->description}}
                            </div>  
                            <div class="col-md-1">
                                <button class="btn btn-primary" data-toggle="modal" data-target="#edit_modal{{$category->id}}">Edit</button>
                                <form method="post" action="/admin/categories/delete">
                                    {{csrf_field()}}
                                    <input type="hidden" name="id" value="{{$category->id}}">
                                    <button type="submit" class="btn btn-danger" style="background-color: #ff0000;">Delete</button>
                                </form>
                                <div class="modal fade" id="edit_modal{{$category->id}}">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                                <h4 class="modal-title">Editing category #{{$category->id}}</h4>
                                            </div>
                                            <form method="post" action="/admin/categories/edit" enctype="multipart/form-data">
                                                {{csrf_field()}}
                                                <input type="hidden" name="id" value="{{$category->id}}">
                                                <div class="modal-body">
                                                    <div class="form-group">
                                                        <label>Name</label>
                                                        <br>
                                                        <input class="form-control" type="text" name="name" value="{{$category->name}}">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Image</label>
                                                        <br>
                                                        <input class="form-control" type="file" name="image" value="{{$category->image}}">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Parent category</label>
                                                        <br>
                                                        <select class="form-control" name="parent_category_id">
                                                            <option disabled selected>Please select parent category</option>
                                                            @foreach($categories as $category1)
                                                                <option value="{{$category1->id}}">
                                                                    {{$category1->name}}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Description</label>
                                                        <br>
                                                        <textarea class="form-control" name="description">{{$category->description}}</textarea>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-primary">Save changes</button>
                                                </div>
                                            </form>
                                        </div><!-- /.modal-content -->
                                    </div><!-- /.modal-dialog -->
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
        </div>
    </div>
@endsection