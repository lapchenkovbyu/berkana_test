@extends('layouts.app')

@section('content')
<div class="container">
    <div class="col-md-2">
        <div class="panel panel-default">
            <div class="panel-heading"><a href="/admin/categories">Categories</a></div>

            <div class="panel-body">
                Categories count : {{\App\Category::count()}}
            </div>
        </div>
    </div>
    <div class="col-md-2">
        <div class="panel panel-default">
            <div class="panel-heading"><a href="/admin/items">Items</a></div>

            <div class="panel-body">
                Categories count : {{\App\Item::count()}}
            </div>
        </div>
    </div>
    <div class="col-md-2">
        <div class="panel panel-default">
            <div class="panel-heading"><a href="/admin/manufacturers">Manufacturers</a></div>

            <div class="panel-body">
                Manufacturers count : {{ \App\Manufacturer::count()}}
            </div>
        </div>
    </div>
    <div class="col-md-2">
        <div class="panel panel-default">
            <div class="panel-heading"><a href="#">Purchases</a></div>

            <div class="panel-body">
                Pending purchases count : {{count($pending_purchases)}}
            </div>
        </div>
    </div>
    <div class="col-md-2">
        <div class="panel panel-default">
            <div class="panel-heading"><a href="/admin/property">Descriptions</a></div>

            <div class="panel-body">
                Pending purchases count : {{count($pending_purchases)}}
            </div>
        </div>
    </div>
</div>
@endsection