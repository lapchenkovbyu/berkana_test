@extends('layouts.app')
@section('content')
@include('admin.property.create_modal')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="text-center">
                    <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#create_property">+ New</button>
                    <h1>Description
                    </h1>
                </div>
                <div class="text-left">
                    <h3>Category
                    </h3>
                    <div class="form-group">
                        <select class="form-control" name="category_id"">
                             <option selected value="all">All categories</option>
                            @foreach($categories as $cat)
                                <option value="{{$cat->id}}">
                                    {{$cat->name}}
                                </option>
                            @endforeach
                        </select>
                        
                    </div>
                </div>

                @foreach($property_name as $prop_name)
                    <div class="panel panel-default">
                        <div class="panel-heading">{{$prop_name->name}}</div>
                        <div class="panel-body">
                            <div class="col-md-10">
                                {{$prop_name->description}}
                            </div>
                            <div class="col-md-2">
                                <button class="btn btn-primary" data-toggle="modal" data-target="#edit_modal_prop{{$prop_name->id}}">Edit</button>
                                <form method="post" action="/admin/property/delete">
                                    {{csrf_field()}}
                                    <input type="hidden" name="id" value="{{$prop_name->id}}">
                                    <button type="submit" class="btn btn-danger" style="background-color: #ff0000;">Delete</button>
                                </form>
                                <div class="modal fade" id="edit_modal_prop{{$prop_name->id}}">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                                <h4 class="modal-title">Editing description #{{$prop_name->id}}</h4>
                                            </div>
                                            <form method="post" action="/admin/property/edit">
                                                {{csrf_field()}}
                                                <input type="hidden" name="id" value="{{$prop_name->id}}">
                                                <div class="modal-body">
                                                    <div class="form-group">
                                                        <label>Name</label>
                                                        <br>
                                                        <input class="form-control" type="text" name="name" value="{{$prop_name->name}}">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Description</label>
                                                        <br>
                                                        <textarea class="form-control" name="description">{{$prop_name->description}}</textarea>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-primary">Save changes</button>
                                                </div>
                                            </form>
                                        </div><!-- /.modal-content -->
                                    </div><!-- /.modal-dialog -->
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
     </div>
    




<table border="1">
	<tr>

		<td>
			@foreach($property_name as $prop_n)
				<div>{{$prop_n->name}}</div>
				<div>{{$prop_n->description}}</div>
			@endforeach
			
		</td>
		<td>
			@foreach($property_value as $prop_val)
				<div>{{$prop_val->property_name_id}}</div>
				<div>{{$prop_val->value}}</div>
			@endforeach
		</td>
		<td>
			@foreach($property_preference as $prop_pref)
				<div>{{$prop_pref->category_id}}</div>
				<div>{{$prop_pref->property_name_description}}</div>
			@endforeach
		</td>
		<td>
			@foreach($item_property as $it_prop)
				<div>{{$it_prop->property_value_id}}</div>
				<div>{{$it_prop->property_name_id}}</div>
				<div>{{$it_prop->item_id}}</div>
			@endforeach
		</td>
	</tr>
</table>

@endsection	