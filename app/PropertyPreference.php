<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyPreference extends Model
{
    protected $fillable = [
        'category_id','property_name_description']
    ;}
