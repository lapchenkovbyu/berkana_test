<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemProperty extends Model
{
    protected $fillable = [
        'property_value_id', 'property_name_id', 'item_id'
    ];
}
