<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Manufacturer;
use App\Item;
use App\Category;
use Illuminate\Database\Eloquent\Model;

class ManyfactyrerController extends Controller
{
     public function admin_index(Request $request)
           {
            // переменная обращается к модели и забирает всё методом ОЛЛ)
            $manufacturer=Manufacturer::all();
            //возвращаем вьюху 
            //DD($manufacturer);
            return (view('admin.manufacturer.index',compact('manufacturer')));
           }

        public function admin_create(Request $request)
        {
        	 // Забираем всё кроме картинки
             $manufacturer=$request->except('_token', 'image');
             //Забираем файл картинки
             $file=$request->file('image');

            // Проверяем есть ли в запросе файл
            if($file) 
            {
            	// Если файл есть, то 
              $path=storage_path('/images');
              $hash=time();
              $extension = $file->getClientOriginalExtension();
              //Перемещение файла из requst при помощи move
              $file->move($path,$hash.'.'.$extension);
              //Путь у файлу с расширением
              $manufacturer['logo']='images/'.$hash.'.'.$extension;

            }

            // Открываем поля модели для заполнения
           
            Model::unguard();
            // Ответом на метод  create будет возврат да или нет (Создалось значение в базе $new_manufacturer )
            $new_manufacturer=Manufacturer::create($manufacturer);
            // Запрет изменения полей модели
            Model::reguard();
            // dd($new_manufacturer);
            return redirect('/admin/manufacturers');
        }

        public function admin_edit(Request $request)
        {
            //dd($request->get('id'));
            $id=$request->get('id');
            $manufacturer_edited=$request->except('id','_token','logo');
            $file=$request->file('image');
            if($file)
            {
                $path=storage_path('/images');
                $hash=time();
                $extension = $file->getClientOriginalExtension();
                $file->move($path,$hash.'.'.$extension);
                $manufacturer['logo']=$path.'/'.$hash.'.'.$extension;
            }
            else
            {
              $manufacturer['logo']='no-img.png';
            }

            $manufacturer=Manufacturer::find($id);
            // dd($manufacturer);
            Model::unguard();
            $manufacturer->update($manufacturer_edited);
            $manufacturer->save();
            Model::reguard();
            return redirect('/admin/manufacturers');
        }

        public function admin_show($id)
        {
            $manufacturer=Manufacturer::find($id);
            // dd($manufacturer);
            $items=Item::all()->where('manufacturer_id',$id);
            foreach ($items as $i)
            {
                $category=Category::find($i->category_id);
            }
            
            // dd($category->id);
            $categories=Category::all();
            //dd($categories);
            $manufacturers=Manufacturer::all();
            return view('admin.manufacturer.show',compact('items','manufacturer','manufacturers','category','categories'));
        }


        public function admin_destroy(Request $request)
         {
           $id=$request->get('id');
           Model::unguard();
           $item=Manufacturer::find($id)->delete();
           Model::reguard();
           return redirect('/admin/manufacturers');
         }

        public function brands()
        {
          $manufacturer=Manufacturer::all();
          return view ('shop.manufacturer',compact('manufacturer'));
        }


}