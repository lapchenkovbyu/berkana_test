<?php

namespace App\Http\Controllers;
use App\Manufacturer;
use App\Item;
use App\Category;
use App\ItemProperty;
use App\PropertyName;
use App\PropertyPreference;
use App\PropertyValue;
use DB;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;

class PropertyController extends Controller
{
    public function admin_index(Request $request)
    {
    	$item_property=ItemProperty::all();
		$property_name=PropertyName::all();
		$property_preference=PropertyPreference::all();
		$property_value=PropertyValue::all();
		$categories=Category::all();
		//dd($property_preference[0]->property_name_description->"$.size" as name);

		$property_preferences = DB::table('property_preferences')
                     ->select(DB::raw('property_name_description->"$.size" as des_size,property_name_description->"$.colors" as des_colors'))
                     ->get();
        //dd($property_preferences);


		return (view('admin.property.index',compact('item_property','property_name','property_preference','property_value','categories')));
    }

    public function admin_create(Request $request)
    {    	
        $property_name=$request->except('_token');
        Model::unguard();
        $new_property_name=PropertyName::create($property_name);
        Model::reguard();
//        return ($new_item);
        return redirect()->back();
    }

    public function admin_edit(Request $request)
    {
          $id=$request->get('id');
          $property_name_edited=$request->except('_token');
          $property_name=PropertyName::find($id);
          Model::unguard();
          $property_name->update($property_name_edited);
          $property_name->save();
          Model::reguard();
        return redirect()->back();
    }

    public function admin_destroy(Request $request)
    {
          $id=$request->get('id');
          Model::unguard();
          $property_name=PropertyName::find($id)->delete();
          Model::reguard();
          return redirect()->back();
    }

    public function admin_show($id)
    {
          // $category=Category::find($id);
          // $subcategories=$category->subcategories()->get();
          // $items=$category->items()->get();
          // // dd($items);
          // $categories=Category::all();
          // $manufacturers=Manufacturer::all();
          // return view('admin.categories.show',compact('category','subcategories','items','categories','manufacturers'));
    }
}
