<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Manufacturer;
use App\Item;
use App\Category;
use App\ItemProperty;
use App\PropertyName;
use App\PropertyPreference;
use App\PropertyValue;
use DB;
use Illuminate\Database\Eloquent\Model;

class CategoryController extends Controller
{
    public function admin_index(Request $request){
        $displayed_categories=Category::where('parent_category_id',null)->get();
        $categories=Category::all();
        return (view('admin.categories.index',compact('displayed_categories','categories')));
    }

    public function admin_create(Request $request){
        $item=$request->except('_token','image');
        $file=$request->file('image');
//        dd($file);
        if($file){
            $path=storage_path('/images');
            $hash=time();
            $extension = $file->getClientOriginalExtension();
            $file->move($path,$hash.'.'.$extension);
            $item['image']='images/'.$hash.'.'.$extension;
        }
        Model::unguard();
        $new_category=Category::create($item);
      //  Model::reguard();
//        return ($new_item);
        return redirect()->back();
    }

    public function admin_edit(Request $request)
    {
          // dd($request);
          $id=$request->get('id');
          $category_edited=$request->except('id','_token','image');
          $file=$request->file('image');
          // dd($file);
        if($file)
          {
              $path=storage_path('/images');
              $hash=time();
              $extension = $file->getClientOriginalExtension();
              $file->move($path,$hash.'.'.$extension);
              $category_edited['image']='images/'.$hash.'.'.$extension;
          }

        else
            {
              $category['image']='no-img.png';
            }

          $category=Category::find($id);
          Model::unguard();
          $category->update($category_edited);
          $category->save();
          Model::reguard();
        return redirect()->back();
    }

    public function admin_destroy(Request $request)
    {
          $id=$request->get('id');
          Model::unguard();
          $category=Category::find($id)->delete();
          Model::reguard();
          return redirect()->back();
    }

    public function admin_show($id)
    {
          $category=Category::find($id);
          $item_property=ItemProperty::all();
          $property_name=PropertyName::all();
          $property_preference=PropertyPreference::all();
          $property_value=PropertyValue::all();
          $subcategories=$category->subcategories()->get();
          $items=$category->items()->get();
          // dd($items);
          $categories=Category::all();
          $manufacturers=Manufacturer::all();
          $property_preference->where('category_id',$id);
          
          $property_preferences = DB::table('property_preferences')
                     ->select(DB::raw('property_name_description->"$.description" as des'))
                     ->where('category_id',$id)
                     ->get();
          //dd($property_preferences);
          foreach ($property_preferences as $prop)
          {
            $property=str_word_count($prop->des, 1);
          }
          //dd($property);
          //dd($property_preferences->des);
          //DD($property);
          return view('admin.categories.show',compact('category','subcategories','items','categories','manufacturers','item_property','property_name','property_preference','property_value','property'));
    }

    public function category_show()
    {
      $category_show=Category::all();
      return view ('shop.category',compact('category_show'));
    }

    
}
