<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Category;
use App\Item;
use App\Manufacturer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Checkout;

class ShopController extends Controller
{
    public function index(Request $request){
        $items=Item::all();
        $categories=Category::where('parent_category_id',null)->get();
        return view('shop.index',compact('items','categories'));
    }

    public function show_category($cat_id)
    {
        $categories=Category::find($cat_id);
        $subcategories=$categories->subcategories()->get();
        $items=$categories->items()->get();
        //dd($categories->image);
        return view('shop.category',compact('categories','subcategories','items'));
    }

    public function add_to_cart(Request $request)
    {
        $id=Auth::user()->id;
        $user_cart=Checkout::where('user_id',$id)->first();
        if($user_cart){
            $checkout=$user_cart;
        }else{

            $checkout=new Checkout();
            $checkout->user_id = $id;
            $checkout->save();
        }

        $item_id=$request->get('item_id');
        $user_id=Auth::user()->id;
        $cart=Cart::where('user_id',$user_id)
            ->where('item_id',$item_id)
            ->where('status','outstanding')
            ->get();
            if($cart->isEmpty())
                {
                    Model::unguard();
                    Cart::create([
                        'item_id'=>$item_id,
                        'user_id'=>$user_id,
                        'cart_id'=>$checkout->id,
                        'quantity'=>1,
                        'status'=>'outstanding'
                    ]);
                    Model::reguard();
                }
            else
                {
                    $cart=$cart->first();
                    $cart->quantity++;
                    $cart->save();
                }
        return(Cart::where('user_id',$user_id)->get());
    }

    public function show_cart(){
        $user_id=Auth::user()->id;
        $cart=Cart::where('user_id',$user_id)->get();
        $total=0;
        foreach ($cart as $key=>$item){
            $item_info=Item::find($item->item_id);
            $cart[$key]->item_info=$item_info;
            $total=+$item->item_info->price*$item->quantity;
        }
        return view('shop.checkout',compact('cart','total'));
    }

    public function purchase(){
        $cart=Auth::user()->current_cart()->get();
        foreach ($cart as $key=>$item){
            $item->status='pending';
            $item->save();
        }
        return view('shop.thank_you');
    }
}
