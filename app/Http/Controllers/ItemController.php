<?php

namespace App\Http\Controllers;

use DB;
use App\Manufacturer;
use App\Item;
use App\Category;
use App\ItemProperty;
use App\PropertyName;
use App\PropertyPreference;
use App\PropertyValue;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class ItemController extends Controller
{
    public function admin_index(Request $request){
        $items=Item::all();
        $categories=Category::all();
        $manufacturers=Manufacturer::all();

        $item_property=ItemProperty::all();
        $property_name=PropertyName::all();
        $property_preference=PropertyPreference::all();
        $property_value=PropertyValue::all();
        
        foreach ($items as $i)
        {
            $i->parent_c = Category::where('id',$i->category_id)->first();
            $i->parent_m = Manufacturer::where('id',$i->manufacturer_id)->first();

        }
        $property_preferences = DB::table('property_preferences')
                     ->select(DB::raw('property_name_description->"$.description" as des'))
                     //->where('category_id',$id)
                     ->get();
        //dd($property_preferences);
        foreach ($property_preferences as $prop)
        {
            $property=str_word_count($prop->des, 1);
        }
        $category=null;
        
        // dd($items);
//        return(csrf_token());
        return (view('admin.items.index',compact('items','categories','manufacturers','category','item_property','property_name','property_preference','property_value','property')));
    }

    public function admin_create(Request $request)
    {
        $item_property=ItemProperty::all();
        $property_name=PropertyName::all();
        $property_preference=PropertyPreference::all();
        $property_value=PropertyValue::all();

        $item=$request->except('_token','image');
        $prop=$request->except('_token','image','name','category_id','description','price');
        //dd($prop);

        


        
        //dd($item);
        $file=$request->file('image');
        if($file)
        {
            $path=storage_path('/images');
            $hash=time();
            $extension = $file->getClientOriginalExtension();
            $file->move($path,$hash.'.'.$extension);
            $item['image']='images/'.$hash.'.'.$extension;
        }
        else
        {
              $item['image']='no-img.png';
        }
        Model::unguard();
        $argument=['name'=>$item['name'],'category_id'=>$item['category_id'],'description'=>$item['description'],'price'=>$item['price'],'image'=>$item['image']];
        $new_item=Item::create($argument);
        Model::reguard();
        foreach ($prop as $k=>$p)
        {
            foreach ($property_name as $p_name)
            {
                if($k==$p_name->name)
                {
                    $new_value=['value'=>$p,'property_name_id'=>$p_name->id];
                    $val=PropertyValue::create($new_value);
                    $item_prop=['property_value_id'=>$val->id,'property_name_id'=>$p_name->id,
                        'item_id'=>$new_item->id];
                    ItemProperty::create($item_prop);
                }
            }
            
            //if($p==property_name)
        }



//        return ($new_item);
        //return redirect('/admin/categories/show/'.$new_item->category_id);
        return redirect()->back();
    }

    public function admin_edit(Request $request){
        $id=$request->get('id');
        $item_edited=$request->except('id','_token','image');
        $file=$request->file('image');
        if($file){
            $path=storage_path('/images');
            $hash=time();
            $extension = $file->getClientOriginalExtension();
            $file->move($path,$hash.'.'.$extension);
            $item['image']=$path.'/'.$hash.'.'.$extension;
        }
//        else{
//            $item['image']='no-img.png';
//        }
        Model::unguard();
        $item=Item::find($id);
        $item->update($item_edited);
        $item->save();
        Model::reguard();
//        return($item);
        return redirect()->back();
    }

    public function admin_destroy(Request $request){
        
        // dd($request->get('id'));
        $id=$request->get('id');
        //$catid=$request->get('category_id');
        Model::unguard();
        $item=Item::find($id);
        $item->delete();
        Model::reguard();
        // return redirect('/admin/categories/show/'.$item->category_id);
        return redirect()->back();
    }

}
