<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyName extends Model
{
    protected $fillable = [
        'name','description'
    ];}
