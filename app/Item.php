<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    public function parent_category(){
        return $this->belongsTo('\App\Category','category_id');
    }
}
