<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyValue extends Model
{
    protected $fillable = [
        'property_name_id','value'
    ];}
