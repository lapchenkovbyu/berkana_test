<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
    
    
Auth::routes();
Route::get('/','ShopController@index');
Route::get('/category/{cat_id}','ShopController@show_category');
Route::get('/item/{i_id}','ShopController@show_category');
Route::post('/add_to_cart', ['uses'=>'ShopController@add_to_cart','middleware'=>'auth']);
Route::get('/cart',['uses'=>'ShopController@show_cart','middleware'=>'auth']);
Route::post('/purchase',['uses'=>'ShopController@purchase','middleware'=>'auth']);
Route::group(['prefix'=>'admin','middleware' => ['auth', 'admin']], function (){
   
    Route::get('/','AdminController@index');

    Route::group(['prefix'=>'purchases'], function(){
        Route::get('/','AdminController@purchases');
        Route::get('/{id}','AdminController@purchases_single');
        Route::post('/action','AdminController@action');
    });
    Route::group(['prefix'=>'items'], function(){
        Route::get('/','ItemController@admin_index');
        Route::post('/create','ItemController@admin_create');
        Route::post('/edit','ItemController@admin_edit');
        Route::post('/delete','ItemController@admin_destroy');
    });
    Route::group(['prefix'=>'categories'], function(){
        Route::get('/','CategoryController@admin_index');
        Route::get('/show/{id}','CategoryController@admin_show');
        Route::post('/create','CategoryController@admin_create');
        Route::post('/edit','CategoryController@admin_edit');
        Route::post('/delete','CategoryController@admin_destroy');
    });
    Route::group(['prefix'=>'manufacturers'], function()
    {
        Route::get('/', 'ManyfactyrerController@admin_index');
        Route::post('/create','ManyfactyrerController@admin_create');
        Route::post('/edit','ManyfactyrerController@admin_edit');
        Route::get('/show/{id}','ManyfactyrerController@admin_show');
        Route::post('/delete','ManyfactyrerController@admin_destroy');
    });
    Route::group(['prefix'=>'property'], function()
    {
        Route::get('/', 'PropertyController@admin_index');
        Route::post('/create','PropertyController@admin_create');
        Route::post('/edit','PropertyController@admin_edit');
        Route::get('/show/{id}','PropertyController@admin_show');
        Route::post('/delete','PropertyController@admin_destroy');
    });


});
Route::get('images/{filename}',function($filename)
{
    $path = storage_path() . '/images/' . $filename;

    if(!File::exists($path)) abort(404);

    $file = File::get($path);
    $type = File::mimeType($path);
    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
});
Route::get('/home', 'HomeController@index');
Route::get('/contact', 'HomeController@contact');
Route::get('/categories','CategoryController@category_show');

Route::group(['prefix'=>'manufacturer'], function()
    {
      Route::get('/', 'ManyfactyrerController@brands');
    });

 Route::group(['prefix'=>'categories'], function()
    {
        
    });